
<div class="container" id="portfolio">
	<h1 style="font-weight:bold">Mobile Development Portfolio</h1>
	<div class="row features-list">
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
			<div class="feature-icon">
				<h1 style="text-align: center;">1</h1>
			</div>
			<h2>Mata Saksi</h2>
			<p>Application to calculate simple calculations such as addition, subtraction, multiplication and division, not applicable science calculator which can calculate certain math formulas.</p>
			<p>
				<a class="btn btn-link" href="detail.php" role="button">View details &raquo;</a>
			</p>
		</div>
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">
			<div class="feature-icon">
				<h1 style="text-align: center;">2</h1>
			</div>
			<h2>Mata Saksi</h2>
			<p>Is an application for calculating surveys and voting input for regional elections in 2019</p>
			<p>
				<a class="btn btn-link" href="detail-matasaksi.php" role="button">View details &raquo;</a>
			</p>
		</div>
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
			<div class="feature-icon">
				<h1 style="text-align: center;">3</h1>
			</div>
			<h2>Unilever Digi PM</h2>
			<p>This project becomes an integral part of the project transformation in the unilever supply chain, for
optimize all the resources to support a more efficient Unilever business
can compete in the global supply chain market.</p>
			<p>
				<a class="btn btn-link" href="detail-digipm.php" role="button">View details &raquo;</a>
			</p>
		</div>
	</div>
</div>
<section id="section-1">
	<div class="container">
	<h1 style="font-weight:bold">Web Development Portfolio</h1>
	<div class="row features-list">
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.5s">
			<div class="feature-icon">
				<h1 style="text-align: center;">1</h1>
			</div>
			<h2>Rekom Partai Hanura</h2>
			<p></p>
			<p>
				<a class="btn btn-link" href="#" role="button">View details &raquo;</a>
			</p>
		</div>
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.7s">
			<div class="feature-icon">
				<h1 style="text-align: center;">2</h1>
			</div>
			<h2>Point of Sale</h2>
			<p></p>
			<p>
				<a class="btn btn-link" href="#" role="button">View details &raquo;</a>
			</p>
		</div>
		<div class="col-sm-4 wow fadeInLeft" data-wow-duration="1.5s" data-wow-offset="10" data-wow-delay="0.9s">
			<div class="feature-icon">
				<h1 style="text-align: center;">3</h1>
			</div>
			<h2>Kebenduman</h2>
			<p></p>
			<p>
				<a class="btn btn-link" href="#" role="button">View details &raquo;</a>
			</p>
		</div>
	</div>
		</div>
	</section>